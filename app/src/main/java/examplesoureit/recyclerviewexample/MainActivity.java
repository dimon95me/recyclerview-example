  package examplesoureit.recyclerviewexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

  public class MainActivity extends AppCompatActivity {

    @BindView(R.id.recycler)
    RecyclerView recyclerView;

      LinearLayoutManager verticalLinearLayoutManager;
      LinearLayoutManager horizontalLinearLayoutManager;

      List<String> stringList = new ArrayList<>();
      RecyclerViewAdapter recyclerViewAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        recyclerViewAdapter = new RecyclerViewAdapter(stringList, getApplicationContext());
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(recyclerViewAdapter);



        prepareData();

    }

    private void prepareData(){
        stringList.add("Canada");
        stringList.add("Ukraine");
        stringList.add("Usa");
        stringList.add("Russia");
        stringList.add("Poland");

        recyclerViewAdapter.notifyDataSetChanged();
    }
}
